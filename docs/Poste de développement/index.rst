.. Commentaire

Installation d'un environnement de développement
******************************************************

Ceci est une documentation permettant décrire une installation pas à pas d'un environnement de développement.
Les logiciels qui seront installés ici seront :

.. toctree::
    :maxdepth: 2

    Git/Git
    IDE/Ide

Prérequis
+++++++++

- Système d'exploitation : Gnu/Linux Ubuntu 19.10
- Mémoire vive : 8 Go
