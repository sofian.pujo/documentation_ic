Installation
============

Installation de Java
#########################

| Afin d'installer Netbeans, et de développer avec le langage Java, il est d'abords nécessaire de l'installer.
| Il est conseillé d'utiliser le JRE1.8 fourni par Openjdk afin d'installer Java.

.. code-block:: shell
   :linenos:

   sudo apt update
   sudo apt install openjdk-8-jdk openjdk-8-jre

Vérifiez que Java est bien installé sur votre machine ensuite.

.. code-block:: shell
   :linenos:

    java -version
   
    openjdk version "1.8.0_222"
    OpenJDK Runtime Environment (build 1.8.0_222-8u222-b10-1ubuntu1~18.04.1-b10)
    OpenJDK 64-Bit Server VM (build 25.222-b10, mixed mode)

Une fois que vous avez vérifiez l'installation de Java, vous devez mettre à jour les variables d'environment permettant de localiser Java et son Jre.

.. code-block:: bash
   :linenos:

    cat >> /etc/environment <<EOL
    JAVA_HOME= /usr/lib/jvm/java-8-openjdk-amd64
    JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
    EOL

Installation de Netbeans
#########################

L'installation de Netbeans peut se faire en passant par le site officiel de Netbeans_.

.. _Netbeans : https://netbeans.org/downloads/8.0.1/