.. _Ide:

================================================
Ide
================================================

| Dans cette section, nous allons voir l'installation d'un Ide adapté pour des développements Java.
| Dans cette optique, nous avons décidé d'utiliser l'IDE Netbeans qui permet l'utilisation de Maven, Java et Git.

.. toctree::

    Ide_Install
    Ide_Use