Utilisation de Git avec un terminal de commande
===============================================

Afin d'utiliser Git à l'aide d'un terminal de commande, il suffit d'utiliser la commande Git et d'utiliser les mots clés nécessaires :

.. code-block:: shell
   :linenos:

   git clone <url du projet>

Cette commande permet de "cloner" un projet dans le répertoire dans lequel vous vous trouvez. Un dossier portant le nom de votre projet sera créé et tout son contenu sera également à l'intérieur de celui-ci.

.. code-block:: shell
   :linenos:

   git branch <nom de branche>

| Cette commande permet de créer une nouvelle branche dans votre projet.
| Une branche permet d'effectuer des développements de manière indépendantes des développements réalisés sur d'autres branche.
| En soit, cela créé un environnement à part pour réaliser une tâche précise.

.. code-block:: shell
   :linenos:

   git checkout <nom de branche>

Cette commande permet de changer de branche et de récupérer le contenu de la branche voulu. Les modifications locales seront alors sauvegardés sous forme de "stash".

.. code-block:: shell
   :linenos:

   git stash

| Cette commande permet d'intérargir avec un "stash" existant ou en créer un.
| Un stash est une sauvegarde locale des fichiers non "commits" dans le repo local Git.
| Les différentes actions possibles sur un stash sont <list, show, drop, pop, apply, clear, create, store> 

.. code-block:: bash
   :linenos:

   git commit -m <message>

Cette commande permet d'envoyer les modifications locales dans le repo local de Git. L'option -m permet de donner un message au commit, qui est obligatoire.

.. code-block:: bash
   :linenos:

   git push

Cette commande permet d'envoyer tout les "commits" sur le serveur afin que celui-ci en prenne compte. Ces modifications seront envoyés sur la branche sur laquelle les commits auront étés réaisés.