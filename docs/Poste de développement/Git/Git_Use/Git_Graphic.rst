Utilisation de Git avec une application graphique
==================================================

| Si vous lisez cette documentation, nous supposons que vous possédez déjà une connaissance de Git permettant son utilisation en ligne de commande.
| Néanmoins, de plus en plus d'applications permettent une utilisation visuelle. La plupars des IDE propose ce genre de moyen, puis d'autres applications sont entièrement dédiées à ceci.
|
| Nous parlerons ici de l'application GitKraken_:

.. _GitKraken: https://www.gitkraken.com/

.. image:: img/gitkraken.png
    :align: center

| GitKraken est donc un logiciel développé par Axosoft permettant une utilisation graphique de Git et de visualiser l'état d'un projet.
| Il dispose d'une version gratuite et d'une version professionnelle. Nous vérons ici la version gratuite.

Utilisation de GitKraken
########################

| Lors de la première ouverture de GitKraken, il vous demandera de vous connecter ou bien de créer un compte.
| Si ce n'est pas déjà le cas, créez un compte ou connectez vous via un des moyens proposés.
|
| Ensuite vous devriez avoir une interface semblable à celle-ci :

.. image:: img/first_gitkraken.png

| En cliquant sur "Clone a repo" ou bien en cliquant sur l'icône de dossier en haut à gauche puis "Clone", une nouvelle interface vous sera proposée.
| Cette interface vous proposera de choisir à partir de quel plateforme vous désirez cloner votre projet Git.
| Dans notre cas, ce sera GitLab.com_. Il faudra donc vous connecter à GitLab.

.. _GitLab.com: GitLab.com

Lors de la connection à GitLab, il est possible que GitKraken vous propose d'ajouter une clé SSH à GitLab. Si vous acceptez, ce sera fait automatiquement, sinon il faudra le faire manuellement.

.. image:: img/clone_gitlab.png
    :align: center

| En cliquant sur "Search Remotes", une liste des différents projets présents sur votre compte GitLab sera proposée.
| Une fois le projet Git cloné, il est temps d'utiliser Git à travers GitKraken.

Présentation de l'interface de GitKraken
#########################################

L'interface de GitKraken est séparée en 3 éléments principaux :

* Le premier élément permet de gérer les différentes branches du projet, que ce soit celles sur le serveur ou elles récupérées. Lors d'un double clic sur l'une d'entre elles, un checkout sera effectué et si besoin, un stash également.

.. image:: img/branch.png
    :align: center

* Le deuxième élément permet de visualiser les différents commits réalisés par chaque membre et sur chaque branche au fil du temps ainsi que leur commentaire. En effectuant un clic droit sur l'un des commits, plusieurs actions seront proposées, tel que créer une branche, réinitialiser la branche à cet instant T, ...

.. image:: img/arbo.png
    :align: center

* Et enfin le dernier élement permet de visualiser les modifications locales à un instant T ou bien de vérifier les modifications sur un commit en particulier.

.. image:: img/commit_detail.png
    :align: center

Utilisation de Git à travers GitKraken
#######################################

Commit un fichier à travers GitKraken
--------------------------------------

Lorsqu'un fichier est modifié, déplacé, créé ou bien supprimé, GitKraken vous l'indiquera à l'aide d'une nouvelle ligne qui apparaîtra au dessus de l'arbre des commits.

.. image:: img/new_modif.png
    :align: center

Lors d'un clic sur cette nouvelle ligne, l'interface de droite se mettra à jour et vous montrera les modifications locales.

.. image:: img/new_modif_commit.png
    :align: center

| De cette interface, il sera possible de stage vos modifications puis de commit en entrant un message ou bien de les supprimer à l'aide de l'icône de poubelle au dessus.
|
| Enfin, un dernière section située au dessous de l'arbre des commits permet d'effectuer plusieurs actions de bases de Git comme :

* Annuler le dernier commit

* Effectuer un pull sur la branche en cours

* Effectuer un push sur la branche en cours

* Créer une branche

* Effectuer un stash sur la branche en cours

* Effectuer un pop sur un stash sélectionné dans l'arborescence

.. image:: img/base_actions.png
    :align: center