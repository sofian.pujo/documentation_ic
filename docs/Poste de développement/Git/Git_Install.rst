Installation
============

Procédure d'installation de Git sur un poste de développement.

Installation de Git
####################

Afin d'installer Git, il suffit de mettre à jour les repositories puis d'installer le paquet "Git"

.. code-block:: shell
   :linenos:

    sudo apt-get update
    sudo apt upgrade
    sudo apt install git

Vous pouvez maintenant utiliser Git.