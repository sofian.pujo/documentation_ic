Installation
============

Procédure d'installation d'Angular sur un poste de développement.

Installation d'Angular
######################

Afin d'installer Angular, il est nécessaire d'avoir installé Nodejs au préalable. Pour ce faire exécutez les commande :

.. code-block:: shell
   :linenos:

    sudo apt install nodejs
    sudo apt install npm

| Npm permet d'installer des modules "Node" et nécessite Nodejs afin d'être exécuté.
| Une fois installé, l'installation d'angular se fait avec la commande : 

.. code-block:: shell
   :linenos:

    npm install -g @angular/cli