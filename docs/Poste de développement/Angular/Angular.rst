.. _Angular:

================================================
Angular
================================================

Angular est un framework opensource basé sur Typescript développée entre autre par Google et une communauté de particuliers et de sociétés.

.. toctree::

    Angular_Install
    Angular_Use