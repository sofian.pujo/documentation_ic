Utilisation
===========

L'Utilisation d'Angular se fait exclusivement en ligne de commande.

Création et exécution d'un projet
##################################

Afin de créer un projet Angular, positionnez-vous dans le dossier de votre choix puis exécutez : 

.. code-block:: shell
   :linenos:

    ng new <nom de l'application>

Une fois exécutée, un nouveau dossier portant le même nom que votre application est créé. Si vous vous rendez dedans et exécutez :

.. code-block:: shell
   :linenos:

    ng serve

Un serveur local sera créé pour tester l'application à travers un navigateur internet. Si un fichier est sauvegardé alors que le serveur ng serve est encore allumé, la modification sera instantanée dans le navigateur.

Il est également possible de compiler votre application avec :

.. code-block:: shell
   :linenos:

    ng build

Le résultat de cette commande sera situé dans un dossier **dist/project-name**