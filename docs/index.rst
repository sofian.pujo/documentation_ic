.. Commentaire

Installation d'un environnement d'intégration continue
*******************************************************

Bonjour, ceci est la documentation disponible dans la version "master".

Ceci est une documentation permettant décrire une installation pas à pas d'un environnement d'intégration continue.
Les logiciels qui seront installés ici seront :

.. toctree::
    :maxdepth: 2

    Serveur d'intégration continue/index
..    Poste de développement/index
