Installation
============

Procédure d'installation de GitLab sur un serveur.

Installation de GitLab
######################

Dans un premier temps, mettre à jour les paquets locaux et installer les paquets nécessaires à l'installation de GitLab :

.. code-block:: shell
   :linenos:

    sudo apt-get update
    sudo apt install ca-certificates curl openssh-server postfix

Ensuite, afin d'installer GitLab, il est nécessaire de télécharger un script permettant son installation.
Placez vous donc dans **/tmp** afin d'y télécharger le script :

.. code-block:: shell
   :linenos:

    cd /tmp
    curl -LO https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh

Vous pouvez, si vous le souhaitez, examiner le script afin d'être sûr qu'il n'est pas mal intentionné.
Ensuite exécutez le.

.. code-block:: shell
   :linenos:

    sudo bash /tmp/script.deb.sh

Ce script va mettre en place le dépôt de GitLab sur votre machine. De cette manière vous pourrez l'installer directement à l'aide d'apt.

.. code-block:: shell
   :linenos:

    sudo apt install gitlab-ce

Paramétrage du pare-feu
########################

Avant de continuer, il est conseillé de vérifier que le pare-feu ne bloque pas les connexions permettant d'accéder et d'utiliser GitLab.

Vous pouvez observer le status actuel de votre pare-feu avec :

.. code-block:: shell
   :linenos:

    sudo ufw status

Et obtenir un résultat similaire à celui-ci :

.. code-block:: shell
   :linenos:

   Output
   Status: active   
   To                         Action      From
   --                         ------      ----
   OpenSSH                    ALLOW       Anywhere                  
   OpenSSH (v6)               ALLOW       Anywhere (v6)

Ajoutez-donc les règles autorisant les transactions dont nous avons besoin :

.. code-block:: shell
   :linenos:

   sudo ufw allow http
   sudo ufw allow https
   sudo ufw allow OpenSSH

Si vous revirifiez le statut de votre pare-feu, vous devriez obtenir un résultat comme celui-ci :

.. code-block:: shell
   :linenos:

   sudo ufw status

.. code-block:: shell
   :linenos:

   Output
   Status: active   
   To                         Action      From
   --                         ------      ----
   OpenSSH                    ALLOW       Anywhere                  
   80/tcp                     ALLOW       Anywhere                  
   443/tcp                    ALLOW       Anywhere                  
   OpenSSH (v6)               ALLOW       Anywhere (v6)             
   80/tcp (v6)                ALLOW       Anywhere (v6)             
   443/tcp (v6)               ALLOW       Anywhere (v6)

Editer le fichier de configuration de GitLab
############################################

L'application GitLab est configurée grâce à un fichier situé sur **/etc/gitlab/gitlab.rb**
Il sera nécessaire d'éditer ce fichier afin d'adapter les configurations pour vous.

Editez le fichier :

.. code-block:: shell
   :linenos:

   sudo nano /etc/gitlab/gitlab.rb

Dans le fichier, recherchez ces lignes :

.. code-block:: shell
   :linenos:

   ##! For more details on configuring external_url see:
   ##! https://docs.gitlab.com/omnibus/settings/configuration.html#configuring-the-external-url-for-gitlab
   external_url 'https://example.com'

Modifier "example.com" par l'adresse de votre serveur ou bien son nom.
Ensuite modifiez également l'adresse mail qui servira à vous contacter en cas de soucis sur votre application :

.. code-block:: shell
   :linenos:

   letsencrypt['contact_emails'] = ['sammy@example.com']

Sauvegardez les modifications et exécutez cette commande qui permettra de recharger les configurations de GitLab : 

.. code-block:: shell
   :linenos:

   sudo gitlab-ctl reconfigure

| Vous pouvez ensuite accédez à l'interface de GitLab !
| Pour en savoir plus sur son utilisation, rendez vous dans notre partie utilisation.