.. _GitLab:

================================================
GitLab
================================================

GitLab est un logiciel libre de forge basé sur git proposant les fonctionnalités de wiki, un système de suivi des bugs, l’intégration continue et la livraison continue. Il peut être utilisé via une interface web en ligne sur https://www.gitlab.com ou bien grâce à une installation personnelle.

.. toctree::

    GitLab_required
    GitLab_install
    GitLab_use