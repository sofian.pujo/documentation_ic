Utilisation
===========

Création du projet
##################

| A votre connexion sur GitLab, vous pourrez apercevoir les différents projets qui vous sont associés.
| Si vous n'en possédez aucun pour le moment, vous pouvez en créer un nouveau en appuyant sur le bouton **New project**

.. image:: img/projects.png

A la création de votre projet, le nom de votre projet sera demandé. Il servira à identifier le projet sur GitLab et composera l'url permettant d'y accéder.

Paramètres du projet
####################

Une fois votre projet créé, nous pouvons modifier ses paramètres. Ils sont accessibles via **Settings** situé en bas à gauche.

    * **Setting -> General** : Cette section vous permettra de changer les paramètres généraux de votre projet tel que son nom, sujet, description, ...
    Vous pourrez également y changer sa visibilité (public ou privé), modifier les autorisations sur les différentes branches, autoriser les merges request, ...

    * **Settings -> Members** : Cette section permettra de gérer les membres du groupe. Vous pouvez en inviter de nouveaux et leur attribuer un rôle.
        * **Guest** : Ce rôle ne permet que d'observer le dépôt, pull le projet, créer des issues. Il est destiné à des personnes ayant accès au projet mais qui ne modifierons rien dans son ensemble.
        * **Reporter** : Ce rôle possède les droits du guest et en plus permet plus de permissions au niveau du management du projet mais ne peut toujours pas toucher directement au code.
        * **Developper** : Ce rôle possède les droits du reporter mais permet en plus d'effectuer des push, créer des tags, des branches, ...
        * **Maintainer** : Ce rôle est simplement le plus haut rôle possible dans un projet. Il possède tout les droits.

    * **Settings -> Integrations** : Permet de pouvoir configurer des webhooks suite à un évenement sur le GitLab.
    * **Settings -> Repository** : Permet de gérer les droits du dépôt. (Branche par défaut, règles de push, ...)
    * **Settings -> CI/CD** : Permet de gérer le CI/CD du projet si un est configuré.
    * **Settings -> Operations** : Permet de gérer les diférentes opérations sur le projet tel que les tracking d'erreurs, incidents, ...

Issues
######

Les issues sur un projet GitLab permettent de pouvoir remonter des anomalies ou bien de proposer des évolutions.
Pour créer une issue sur GitLab il suffit de cliquer sur **New issue** et de remplir le formulaire.

.. image:: img/issue.png

De plus, une issue permettra au développeur de pouvoir créer une merge request afin de pouvoir corriger l'anomalie remontée sans affecter la branche de développement en cliquant sur le bouton.

.. image:: img/issue_merge.png

Merge request
#############

| Une merge request permet à un développeur de fusionner des modifications effectuées sur une branches et une branche cible.
| Une norme sur GitLab veut qu'une merge request précédée de **WIP** (Work In Progress) ne puisse pas être fermée ou fusionnée.
| Il faudra d'abords résoudre le **WIP** afin de fusionner.

.. image:: img/wip.png

Si des conflits sont détecter lors d'un merge, GitLab vous invitera a résoudre les conflits avec un éditeur externe.

GitLab CI/CD
############

| Le GitLab CI/CD (Continuous Integration / Continuous Deploymoent) permet de créer un cycle d'intégration continue sur votre projet.
| Celui-ci ne sera activé que si vous possédez un fichier nommé "**.gitlab-ci.yml**" à la racine de votre projet.
| Nous vous invitions à lire la documentation de GitLab à ce sujet : CI_CD_

.. _CI_CD: https://docs.gitlab.com/ee/ci/
