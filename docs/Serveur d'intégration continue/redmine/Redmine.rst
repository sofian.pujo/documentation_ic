.. _Redmine:

================================================
Redmine
================================================

| Redmine est une application Web libre permettant d'effectuer entre autre de la gestion de projets, ticketing, rapports, saisie de temps, ...

.. toctree::

    Redmine_required
    Redmine_install
    Redmine_use