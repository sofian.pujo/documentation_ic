Installation
============

| Procédure d'installation de Redmine sur un serveur.

| Nous allons installer Redmine à partir de Docker.

| Afin d'installer Redmine à l'aide d'un Docker, il suffit d'exécuter cette commande :

.. code-block:: console
   :linenos:

   docker run -d --name some-redmine redmine

Une fois fait, vous pourrez accédez à Redmine depuis http://localhost:8080