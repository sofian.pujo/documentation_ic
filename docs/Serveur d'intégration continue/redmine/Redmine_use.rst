Utilisation
===========

Lors de votre première connexion sur Redmine, vous devriez arriver sur une page vierge avec simplement un en-tête permettant de cliquer sur diverses catégories.

.. image:: img/entete.png

Création d'un projet
#####################

Dans un premier temps cliquez sur "Projet" et créez votre projet.

.. image:: img/project_creation.png

Administration
###############

Une fois votre projet créé vous pourrez accéder au menu "Administration" situé dans l'en-tête de Redmine.

.. image:: img/administration.png

| Depuis cette interface vous pourrez gérer la plupars des paramètres de votre Redmine et de vos projets.

Création de rôles
+++++++++++++++++++

| Nous allons dans un premier temps gérer les rôles disponibles sur ce projet. Cliquez sur **Rôles et permissions**.
| Depuis cette interface il vous sera possible de créer de nouveaux rôles, ceux-ci nécessiteront des droits que vous pourrez définir à la création ou l'édition.

Création des utilisateurs
++++++++++++++++++++++++++

| Une fois les rôles créés, vous pouvez dès à présent vous rendre dans la section **Utilisateurs**.
| La liste des utilisateurs inscrits sur votre Redmine est affichée, vous pouvez en créer un nouveau ou bien cliquer sur l'un d'entre eux pour éditer ses paramètres :

.. image:: img/new_user.png

**Si vous éditez un utilisateur, 2 onglets apparaîtront en haut du formulaire afin de pouvoir lui associer des projets.**

.. image:: img/edit_user.png

En cliquant sur l'onglet **Projet** vous pourrez associer un projet à l'utilisateur ainsi qu'un rôle.

.. image:: img/project_roles.png

Création des trackers
++++++++++++++++++++++

| Un tracker permettra de catégoriser une demande sur Redmine. En général les trackers les plus répandus sont **Bug** et **Evolution**.
| Lors de la création d'un tracker, il vous sera demandé un nom ainsi qu'un statut par défaut.
| Il faudra également associer ce tracker aux projets sur lesquels vous souhaitez l'utiliser.

.. image:: img/new_tracker.png

Création des statuts des demandes
++++++++++++++++++++++++++++++++++

| Un statut d'une demande permettra d'indiquer son état. Il pourra être modifié au fur et à mesure de son avancé.
| Nous aurons par exemple des statuts tels que **Ouvert**, **En cours**, **Corrigé**, **Terminé**, et bien d'autres selon les besoins.
| Lors de la création d'une demande, il vous sera demandé un nom et si la demande doit être fermée ou non lorsqu'une demande porte ce statut.

Création d'un Workflow
+++++++++++++++++++++++

| Un workflow dans Redmine permettra d'établir un ordre d'assignation des statuts des demandes.
| Ce workflow pourra être modifié en fonction de chaque rôles.
| Voici un exemple :

.. image:: img/workflow.png

| Dans cet exemple-ci, un **Développeur** pourra passer une demande d'un statut :

    * **Ouvert** à **En cours** ou bien **Fermé**

    * **En cours** à **Terminé** ou bien **Fermé**

    * **Terminé** à **Fermé**

Une fois le workflow édité vous pourrez cliquer sur **Sauvegarder** afin de le sauvegarder.