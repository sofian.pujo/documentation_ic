.. _Jenkins:

================================================
Jenkins
================================================

Jenkins est une plateforme prenant place dans plusieurs phases d'une intégration continue.
| Elle permet entre autre de pouvoir automatiser des compilations ou générations de builds.

.. toctree::

    Jenkins_required
    Jenkins_install
    Jenkins_use