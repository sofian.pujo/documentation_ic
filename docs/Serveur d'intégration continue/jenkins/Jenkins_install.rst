Installation
============

Procédure d'installation de Jenkins sur un serveur.

Nous allons installer Jenkins à partir de Docker.

.. code-block:: console
   :linenos:

   docker pull jenkins/jenkins

Et exécutez Jenkins :

.. code-block:: console
   :linenos:

   docker run -p 8080:8080 --name=jenkins-master jenkins/jenkins

| Vous pourrez ensuite y accéder sur http://localhost:8080
| A votre première connexion sur Jenkins, il vous demandera un mot de passe situé dans un fichier texte dans le docker.
| Pour accéder à ce fichier, exécutez la commande :

.. code-block:: console
   :linenos:

   docker exec -t -i jenkins-master /bin/bash
   cat /var/jenkins_home/secrets/initialAdminPassword

Et entrez ce mot de passe dans l'interface :

.. image:: img/preparation.png

| Jenkins vous demandera ensuite quels plugins installer sur la machine. Sélectionnez les plugins suggérés, vous pourrez les changer plus tard au besoin.
| Vous n'aurez plus qu'à attendre la fin de l'installation.

.. image:: img/plugins.png