Utilisation
===========

| Lors de votre première connexion sur Jenkins en tant qu'admin il vous sera demander de créer un job.
| Cliquez sur **créer un nouveau job**

.. image:: img/premiere_connexion.png

Ensuite Jenkins demandera quel genre de job créer. 

.. image:: img/job_type.png

Nous verrons les 2 genres de projet qui nous intéresse : Projet Free-style, projet Pipeline.

Projet Free-style
#################

| Un projet "Free-style" dans Jenkins vous permettra de pouvoir réaliser les différentes étapes de build directements à partir de la configuration du projet.
| Vous pourrez utiliser directement les plugins installés sur Jenkins, en installer des nouveaux, et écrire vos propres scripts shells si besoin.
| Il est également possible de donner un projet Git ou SVN à cloner afin de le compiler.

.. image:: img/free_style_git.png

Lors de la configuration de ce type de projet il est également possible de décrire ce qui déclenchera un build, modifier les options de build et de choisir les actions à exécuter lors du build.

.. image:: img/build.png

| Une fois avoir paramétré le build nous pouvons indiquer quoi faire une fois que la phase de build est terminée.
| Cette phase est très utile par exemple pour archiver et consulter des résultats de tests.

.. image:: img/after_build.png

Le projet étant configuré, il est possible de l'exécuter manuellement en cliquant sur **Lancer un build** disponible sur la gauche.

.. image:: img/lancer_build.png

Il sera possible de suivre son avancée en direct sur l'interface de Jenkins et d'avoir les logs de chaque phase de build.

Projet Pipeline
###############

| Une projet Pipeline est plus personnalisable qu'un projet Free-style. En effet, il est possible de créer un fichier **jenkinsfile** afin de pouvoir créer notre propre script de build.
| Ce script peut être stocké sur un gestionnaire de version. Il suffirat de spécifier le dépôt ainsi que le chemin du script.

.. image:: img/jenkinsfile_def.png

| Un script jenkins est composé de plusieurs fragments.

    - Pipeline : Ce fragment est la racine du script, il contiendra tout les autres blocs
    - Stages : Décrit les différentes étapes du build de votre job
    - Steps : Décrit une ou plusieurs étapes lors d'un stage dans un build

.. code-block:: json
   :linenos:

   pipeline {
        agent any

        stages {
            stage('Build') {
                steps {
                    echo 'Building..'
                }
            }
            stage('Test') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Deploy') {
                steps {
                    echo 'Deploying....'
                }
            }
        }
    }

Il est également possible de spécifier une image docker, une instance ec2 aws, un kubernetes ...

Pour plus d'information sur les scripts pipeline veuillez consulter la documentation officielle : https://jenkins.io/doc/book/pipeline/syntax/