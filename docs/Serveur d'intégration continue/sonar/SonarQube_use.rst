Utilisation
===========

Créez un fichier de configuration à la racine de votre projet qui s'appelera : sonar-project.properties
Dans ce fichier, insérez ceci :

.. code-block:: python
   :linenos:

    sonar-project.properties
    # must be unique in a given SonarQube instance
    sonar.projectKey=my:project

    # --- optional properties ---

    # defaults to project key
    #sonar.projectName=My project
    # defaults to 'not provided'
    #sonar.projectVersion=1.0
    
    # Path is relative to the sonar-project.properties file. Defaults to .
    #sonar.sources=.
    
    # Encoding of the source code. Default is default system encoding
    #sonar.sourceEncoding=UTF-8

Utilisation de Sonnar-scanner
#############################

Si un fichier sonar-project.properties ne peut pas être créé à la racine du projet, il y a plusieurs autres moyens :

Les propriétés peuvent être spécifiées directement en ligne de commande. Par exemple :

.. code-block:: shell
   :linenos:
   
    sonar-scanner -Dsonar.projectKey=myproject -Dsonar.sources=src1

La propriété **project.settings** peut être utilisée pour spécifier l'emplacement du fichier de configuration (Cette option n'est pas compatible avec la propriété **sonar.projectBaseDir**). Par exemple :

.. code-block:: shell
   :linenos:

    sonar-scanner -Dproject.settings=../myproject.properties

Le dossier racine du projet qui doit être analysé peut être spécifié via la propriété **sonar.projectBaseDir** depuis SonarScanner 2.4. Ce dossier doit contenir un fichier sonar-project.properties if **sonar.projectKey** n'est pas spécifié en ligne de commande.

Utilisation de l'interface SonarQube
####################################

* Une fois connectée sur l'interface de SonarQube, les différents projets câblés seront affichés.

.. image:: img/projets.png

* Lors du clic sur l'un des projets, les différentes informations sur le dernier scan effectué seront affichés.

.. image:: img/infos.png

Ces informations indiquent s'il y a des vulnérabilités dans le code ou bien si des bonnes pratiques sont recommandés.

* Lors du clic sur l'une de ces informations comme par exemple les vulnérabilités, l'interface nous affichera les différentes vulnérabilités trouvés, nous indiquera la ligne dans le code et même une suggestion de correction. Il est bien sûr possible de cliquer sur chaque vulnérabilité trouvé afin d'avoir des détails.

.. image:: img/vulne.png