Installation
============

Installation et lancement d'un conteneur SonarQube :

Il est possible d'installer SonarQube grâce à Docker.
Afin de se faire, il faut dans un premier temps récupérer l'image docker de SonarQube.

.. code-block:: console
   :linenos:

   docker pull sonarqube:latest

Puis de lancer SonarQube en spécifiant le port (ici 9000)
    
.. code-block:: console
   :linenos:

   docker run -d --name sonarqube -p 9000:9000 sonarqube

Nous allons maintenant installer Sonar Scanner disponible en téléchargement à cette adresse : https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.2.0.1873-linux.zip

* Placez le fichier télécharger dans le dossier de votre choix et dézippez le. Dans les prochaines étapes nous l'appellerons **$install_directory**.

* Dans le fichier **$install_directory**/conf/sonar-scanner.properties mettez à jour les informations par rapport à votre serveur SonarQube (Décommenttez si les valeurs par défaut sont changées) :

.. code-block:: python
   :linenos:

    #----- Default SonarQube server
    #sonar.host.url=http://localhost:9000

* Ajoutez le chemin **$install_directory**/bin dans vos variables d'environnement.

* Vérifiez votre installation en ouvrant un nouveau shell et exécutez la commande :

.. code-block:: shell
   :linenos:

   sonar-scanner -h

Vous devriez avoir un retour comme celui-ci :

.. code-block:: shell
   :linenos:

   usage: sonar-scanner [options]

    Options:
    -D,--define <arg>     Define property
    -h,--help             Display help information
    -v,--version          Display version information
    -X,--debug            Produce execution debug output