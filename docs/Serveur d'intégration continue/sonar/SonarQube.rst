.. _SonarQube:

================================================
SonarQube
================================================

SonarQube est un logiciel libre permettant de mesurer la qualité du code source en continu.

.. toctree::

    SonarQube_required
    SonarQube_install
    SonarQube_use