.. Commentaire

Installation d'un environnement d'intégration continue
******************************************************

Ceci est une documentation permettant décrire une installation pas à pas d'un environnement d'intégration continue.
Les logiciels qui seront installés ici seront :

.. toctree::
    :maxdepth: 2

    docker/Docker
    sonar/SonarQube
    gitlab/GitLab
    redmine/Redmine
.. jenkins/Jenkins

Prérequis
+++++++++

- Système d'exploitation : Gnu/Linux Ubuntu 19.10
- Mémoire vive : 8 Go
