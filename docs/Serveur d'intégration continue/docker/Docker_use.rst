Utilisation
===========

*Les commandes ne sont données qu'à titre d'exemple*

Télécharger une image :

.. code-block:: console
   :linenos:

    # docker pull nom_image

Supprimer une image :

.. code-block:: console
   :linenos:

    # docker image rm nom_image

Installer et lancer l'image :

.. code-block:: console
   :linenos:

    # docker run -d --name nom_conteneur -p 9000:9000 nom_image

Voir les conteneurs :

.. code-block:: console
   :linenos:

    # docker ps -a

Lancer un conteneur :

.. code-block:: console
   :linenos:

    # docker start nom_conteneur

Arreter un conteneur :

.. code-block:: console
   :linenos:

    # docker stop nom_conteneur

Supprimer un conteneur :

.. code-block:: console
   :linenos:

    # docker rm nom_conteneur

Pour aller plus loin, voir `la documentation officielle`_.

.. _`la documentation officielle`: https://docs.docker.com/