Installation
============

Mettre à jour APT :

.. code-block:: console
   :linenos:

   sudo apt-get update

Installation des paquets permettant à apt d'utiliser les repo via https :

.. code-block:: console
   :linenos:

   sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

Ajout de la clé GPG :

.. code-block:: console
   :linenos:

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Verifier le fingerprint :

.. code-block:: console
   :linenos:

   sudo apt-key fingerprint 0EBFCD88

Ajouter le repo APT :

.. code-block:: console
   :linenos:

   sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable"

Mettre à jour APT :

.. code-block:: console
   :linenos:

   sudo apt-get update

Installer Docker :

.. code-block:: console
   :linenos:

   sudo apt-get install docker-ce