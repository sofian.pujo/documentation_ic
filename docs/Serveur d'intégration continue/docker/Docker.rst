.. _Docker:

================================================
Docker
================================================

Docker est un logiciel libre à mi-chemin entre la virtualisation applicative et l'automatisation. Il permet de manipuler des conteneurs de logiciels.
Contrairement aux autres systèmes de virtualisation, Docker n’embarque pas un système d’exploitation invité mais ne s’occupe que de la partie haut niveau.

Il utilise le noyau de l'hôte et ne fait fonctionner que le strict nécessaire sur les invités.
Docker dispose aussi d'un hub d'images toutes prêtes à télécharger, il sagit de dockerhub.

.. toctree::

    Docker_required
    Docker_install
    Docker_use