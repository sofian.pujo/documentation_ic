# Installation d'un environnement d'intégration continue

Ceci est une documentation permettant décrire une installation pas à pas d'un environnement d'intégration continue.
Les logiciels qui seront installés ici seront :

* [Docker](docs/docker/Docker)
* [Sonar](docs/sonar/SonarQube)
* [GitLab](docs/gitlab/GitLab)

## Prérequis

- Système d'exploitation : Gnu/Linux Ubuntu 1
- 8 Go de RAM